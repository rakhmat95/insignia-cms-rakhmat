import React, { useState } from "react";
import ReactModal from "react-modal";
import { AiFillCloseCircle } from "react-icons/ai";
import FormTravel from "../Molecules/FormTravel";
import { Colors } from "../../constants";

function ModalFormTravel({
  title,
  defaultValue,
  isOpen,
  modalStyles,
  closeModal,
  onSubmit,
}) {
  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={closeModal}
      style={
        modalStyles ?? {
          content: {
            display: "flex",
            flex: 1,
            flexDirection: "row",
            backgroundColor: Colors.PRIMARY,
            borderRadius: 16,
            justifyContent: "center",
            marginRight: 100,
            marginLeft: 100,
          },
        }
      }
    >
      <div onClick={closeModal}>
        <AiFillCloseCircle size={36} color={"white"} />
      </div>
      <FormTravel
        defaultValue={defaultValue}
        title={title}
        onSubmit={onSubmit}
      />
    </ReactModal>
  );
}

export default ModalFormTravel;
