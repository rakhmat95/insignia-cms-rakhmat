import React, { useState } from "react";
import ReactModal from "react-modal";
import { AiFillCloseCircle } from "react-icons/ai";
import styled from "styled-components";

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
`;

const WrapperButton = styled.section`
  padding: 1em;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Button = styled.button`
  font-size: 1.5em;
  text-align: center;
  margin-right: 18px;
`;

function Modal({ title, isOpen, modalStyles, closeModal, onConfirm }) {
  return (
    <ReactModal isOpen={isOpen} onRequestClose={closeModal} style={modalStyles}>
      <div onClick={closeModal}>
        <AiFillCloseCircle size={24} color={"red"} />
      </div>
      <Title>{title}</Title>
      <WrapperButton>
        <Button onClick={onConfirm}>Yes</Button>
        <Button onClick={closeModal}>No</Button>
      </WrapperButton>
    </ReactModal>
  );
}

export default Modal;
