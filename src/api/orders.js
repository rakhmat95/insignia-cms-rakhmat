import axios from "axios";
import { API_URL } from "../constants";

import { configAuth, handler } from "./utils";

export async function createOrder(data) {
  try {
    const res = await axios.post(
      `${API_URL}/orders`,
      data?.params,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function createOrderDetail(data) {
  try {
    const res = await axios.post(
      `${API_URL}/order-details`,
      data?.params,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function find(data) {
  try {
    let params = "";
    // if (data?.params !== "") {
    //   params = `&populate[filters][invoice_number][$containsi]=${
    //     data?.params
    //   }&populate[filters][invoice_number][$containsi]=${
    //     isNaN(data?.params) ? "O" : data?.params
    //   }&populate[filters][invoice_number][$containsi]=${data?.params}`;
    // }

    const res = await axios.get(
      `${API_URL}/order-details?populate[0]=order_id&populate[1]=order_id.customer_id&populate[2]=travel_package_id.image${params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function findOne(data) {
  try {
    const res = await axios.get(
      `${API_URL}/order-details/${data?.params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function updateOrders(data) {
  try {
    const res = await axios.put(
      `${API_URL}/orders/${data?.id}`,
      data?.params,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function updateOrderDetails(data) {
  try {
    const res = await axios.put(
      `${API_URL}/order-details/${data?.id}`,
      data?.params,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function removeOrders(data) {
  try {
    const res = await axios.delete(
      `${API_URL}/orders/${data?.params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function removeOrderDetails(data) {
  try {
    const res = await axios.delete(
      `${API_URL}/order-details/${data?.params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}
