import { createModel } from "@rematch/core";
import { getPersistor } from "@rematch/persist";
import * as api from "../../api/auth";

export const authToken = createModel()({
  state: null,
  reducers: {
    updateData(state, payload) {
      state = payload;
      return state;
    },
    resetData(state) {
      state = null;
      return state;
    },
  },
  effects: (dispatch) => ({
    async login({ identifier, password }) {
      const response = await api.login({
        identifier,
        password,
      });
      dispatch.authToken.updateData(response);
      return response;
    },
    async register({ username, name, email, password }) {
      const response = await api.register({
        username,
        email,
        password,
      });
      dispatch.authToken.updateData(response);
      return response;
    },
    async logout() {
      getPersistor().purge();
      dispatch.authToken.resetData();
    },
  }),
});
