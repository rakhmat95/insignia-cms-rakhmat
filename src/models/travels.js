import { createModel } from "@rematch/core";
import * as api from "../api/travelPackages";

export const travels = createModel()({
  state: {
    travel: null,
    travels: [],
  },
  reducers: {
    updateTravelData(state, payload) {
      state.travel = payload;
      return state;
    },
    updateTravelsData(state, payload) {
      state.travels = payload;
      return state;
    },
    resetTravelData(state) {
      state.travel = null;
      return state;
    },
    resetTravelsData(state) {
      state.travels = [];
      return state;
    },
  },
  effects: (dispatch) => ({
    async create({ token, params }) {
      const response = await api.create({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
    async find({ token, params }) {
      const response = await api.find({
        token: token,
        params: params,
      });
      const data = response?.data;
      dispatch.travels.updateTravelsData(data);
      return data;
    },
    async findOne({ token, params }) {
      const response = await api.findOne({
        token: token,
        params: params,
      });
      const data = response?.data;
      dispatch.travels.updateTravelData(data);
      return data;
    },
    async updates({ token, params, id }) {
      const response = await api.updates({
        token: token,
        params: params,
        id: id,
      });
      const data = response?.data;
      return data;
    },
    async delete({ token, params }) {
      const response = await api.remove({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
  }),
});
