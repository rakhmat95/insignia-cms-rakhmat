import { createModel } from "@rematch/core";
import * as api from "../api/customers";

export const customers = createModel()({
  state: {
    customer: null,
    customers: [],
  },
  reducers: {
    updateCustomerData(state, payload) {
      state.customer = payload;
      return state;
    },
    updateCustomersData(state, payload) {
      state.customers = payload;
      return state;
    },
    resetCustomerData(state) {
      state.customer = null;
      return state;
    },
    resetCustomersData(state) {
      state.customers = [];
      return state;
    },
  },
  effects: (dispatch) => ({
    async create({ token, params }) {
      const response = await api.create({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
    async find({ token, params }) {
      const response = await api.find({
        token: token,
        params: params,
      });
      const data = response?.data;
      dispatch.customers.updateCustomersData(data);
      return data;
    },
    async findOne({ token, params }) {
      const response = await api.findOne({
        token: token,
        params: params,
      });
      const data = response?.data;
      dispatch.customers.updateCustomerData(data);
      return data;
    },
    async updates({ token, params, id }) {
      const response = await api.updates({
        token: token,
        params: params,
        id: id,
      });
      const data = response?.data;
      return data;
    },
    async delete({ token, params }) {
      const response = await api.remove({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
  }),
});
