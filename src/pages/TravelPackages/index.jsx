import React, { useState, useEffect } from "react";
import DashboardHeader from "../../components/Molecules/DashboardHeader";

import all_orders from "../../constants/orders";
import { calculateRange, sliceData } from "../../utils/table-pagination";

import "../styles.css";
import Table from "react-bootstrap/Table";
import { useDispatch, useSelector } from "react-redux";
import { BiEdit, BiTrash } from "react-icons/bi";
import useDebounce from "../../hooks/useDebounce";
import styled from "styled-components";
import ModalFormTravel from "../../components/Organisms/ModalFormTravel";
import Modal from "../../components/Atoms/Modal";
import { BASE_URL } from "../../constants";

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    width: 300,
    height: 200,
    marginRight: "-50%",
    backgroundColor: "#FFF",
    transform: "translate(-50%, -50%)",
    borderRadius: 10,
  },
};

const Control = styled.td`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

function TravelPackages() {
  const authToken = useSelector((rootState) => rootState.authToken);
  const { travel, travels } = useSelector((rootState) => rootState.travels);

  const dispatch = useDispatch();

  const [search, setSearch] = useState("");
  const [orders, setOrders] = useState(all_orders);
  const [page, setPage] = useState(1);
  const [pagination, setPagination] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const debouncedSearch = useDebounce(search, 500);

  const [openModalAddTravel, setOpenModalAddTravel] = useState(false);
  const [openModalEditTravel, setOpenModalEditTravel] = useState(false);
  const [openModalDeleteTravel, setOpenModalDeleteTravel] = useState(false);

  useEffect(() => {
    setPagination(calculateRange(all_orders, 5));
    setOrders(sliceData(all_orders, page, 5));
  }, []);

  useEffect(() => {
    dispatch.travels.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
  }, [debouncedSearch]);

  // Search
  const __handleSearch = (event) => {
    setSearch(event.target.value);
    if (event.target.value !== "") {
      let search_results = orders.filter(
        (item) =>
          item.first_name.toLowerCase().includes(search.toLowerCase()) ||
          item.last_name.toLowerCase().includes(search.toLowerCase()) ||
          item.product.toLowerCase().includes(search.toLowerCase())
      );
      setOrders(search_results);
    } else {
      __handleChangePage(1);
    }
  };

  // Change Page
  const __handleChangePage = (new_page) => {
    setPage(new_page);
    setOrders(sliceData(all_orders, new_page, 5));
  };

  const onEdit = (data) => {
    setSelectedItem(data);
    setOpenModalEditTravel(true);
  };

  const onDelete = (data) => {
    setSelectedItem(data);
    setOpenModalDeleteTravel(true);
  };

  const onSubmitAdd = async (data) => {
    await dispatch.travels.create({
      token: authToken?.jwt,
      params: { data },
    });
    dispatch.travels.resetTravelsData();
    await dispatch.travels.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalAddTravel(false);
  };

  const onSubmitEdit = async (data) => {
    await dispatch.travels.updates({
      token: authToken?.jwt,
      params: { data },
      id: selectedItem?.id,
    });
    dispatch.travels.resetTravelsData();
    await dispatch.travels.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalEditTravel(false);
  };

  const onSubmitDelete = async () => {
    await dispatch.travels.delete({
      token: authToken?.jwt,
      params: selectedItem?.id,
    });
    dispatch.travels.resetTravelsData();
    await dispatch.travels.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalDeleteTravel(false);
  };

  return (
    <div className="dashboard-content">
      <ModalFormTravel
        title={"Add Travel"}
        isOpen={openModalAddTravel}
        closeModal={() => setOpenModalAddTravel(false)}
        onSubmit={onSubmitAdd}
      />
      <ModalFormTravel
        title={"Edit Travel"}
        defaultValue={selectedItem?.attributes}
        isOpen={openModalEditTravel}
        closeModal={() => setOpenModalEditTravel(false)}
        onSubmit={onSubmitEdit}
      />
      <Modal
        title={`Are you sure to delete id: ${selectedItem?.id}?`}
        isOpen={openModalDeleteTravel}
        modalStyles={modalStyles}
        closeModal={() => setOpenModalDeleteTravel(false)}
        onConfirm={onSubmitDelete}
      />

      <DashboardHeader
        btnText="Add Travel Package"
        onClick={() => setOpenModalAddTravel(true)}
      />

      <div className="dashboard-content-container">
        <div className="dashboard-content-header">
          <h2>Travel Packages List</h2>
          <div className="dashboard-content-search">
            <input
              type="text"
              value={search}
              placeholder="Search.."
              className="dashboard-content-input"
              onChange={(e) => __handleSearch(e)}
            />
          </div>
        </div>

        <Table responsive>
          <thead>
            <th>ID</th>
            <th>NAME</th>
            <th>DESCRIPTION</th>
            <th>PRICE</th>
            <th>IMAGE</th>
            <th></th>
          </thead>

          {travels?.length !== 0 ? (
            <tbody>
              {travels?.map((item, index) => (
                <tr key={index}>
                  <td>
                    <span>{item.id}</span>
                  </td>
                  <td>
                    <span>{item.attributes.name}</span>
                  </td>
                  <td>
                    <span>{item.attributes.description}</span>
                  </td>
                  <td>
                    <span>{item.attributes.price}</span>
                  </td>
                  <td>
                    <img
                      src={
                        BASE_URL + item.attributes.image?.data?.attributes?.url
                      }
                      alt="canceled-icon"
                      className="dashboard-content-icon"
                    />
                  </td>
                  <Control>
                    <div onClick={() => onEdit(item)}>
                      <BiEdit />
                    </div>
                    <div onClick={() => onDelete(item)}>
                      <BiTrash />
                    </div>
                  </Control>
                </tr>
              ))}
            </tbody>
          ) : null}
        </Table>

        {travels.length !== 0 ? (
          <div className="dashboard-content-footer">
            {pagination.map((item, index) => (
              <span
                key={index}
                className={item === page ? "active-pagination" : "pagination"}
                onClick={() => __handleChangePage(item)}
              >
                {item}
              </span>
            ))}
          </div>
        ) : (
          <div className="dashboard-content-footer">
            <span className="empty-table">No data</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default TravelPackages;
