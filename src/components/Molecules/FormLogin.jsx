import React from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import FormInput from "../Atoms/FormInput";
import { Colors } from "../../constants";

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10em;
`;

const Title = styled.h2`
  color: whitesmoke;
  margin-bottom: 16px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2.5em;
  background-color: ${Colors.PRIMARY};
  border-radius: 10px;
  width: 50%;
`;

const FormSubmit = styled.input`
  padding: 10px;
  border-radius: 10px;
  background-color: white;
  font-size: medium;
  font-weight: bold;
`;

const Register = styled.div`
  margin-top: 16px;
  text-decoration: underline;
  color: white;
  font-weight: bold;
`;

function FormLogin({ onSubmit, onRegister }) {
  const { setValue, register, handleSubmit } = useForm();

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Title>Login Akun</Title>
        <FormInput
          required={true}
          {...register("identifier", { required: true })}
          placeholder="Email"
          type={"email"}
          onChange={(e) => setValue("identifier", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("password", { required: true })}
          placeholder="Password"
          type={"password"}
          onChange={(e) => setValue("password", e.target.value)}
        />
        <FormSubmit type="submit" value="Login" />
        <Register onClick={onRegister}>Register</Register>
      </Form>
    </Wrapper>
  );
}

export default FormLogin;
