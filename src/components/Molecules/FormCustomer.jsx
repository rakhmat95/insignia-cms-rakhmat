import React, { useEffect } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import FormInput from "../Atoms/FormInput";
import { Colors } from "../../constants";

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

const Title = styled.h2`
  color: whitesmoke;
  margin-bottom: 16px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${Colors.PRIMARY};
  border-radius: 10px;
  width: 50%;
`;

const FormSubmit = styled.input`
  padding: 10px;
  border-radius: 10px;
  background-color: white;
  font-size: medium;
  font-weight: bold;
`;

function FormCustomer({ title, defaultValue, onSubmit }) {
  const { setValue, register, handleSubmit } = useForm();

  useEffect(() => {
    if (defaultValue) {
      setValue("name", defaultValue?.name);
      setValue("phone", defaultValue?.phone);
      setValue("email", defaultValue?.email);
      setValue("address", defaultValue?.address);
    }
  }, [defaultValue]);

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Title>{title}</Title>
        <FormInput
          required={true}
          {...register("name", { required: true })}
          placeholder="Name"
          defaultValue={defaultValue?.name}
          onChange={(e) => setValue("name", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("phone", { required: true })}
          placeholder="Phone"
          defaultValue={defaultValue?.phone}
          onChange={(e) => setValue("phone", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("email", { required: true })}
          placeholder="Email"
          type={"email"}
          defaultValue={defaultValue?.email}
          onChange={(e) => setValue("email", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("address", { required: true })}
          placeholder="Address"
          defaultValue={defaultValue?.address}
          onChange={(e) => setValue("address", e.target.value)}
        />
        <FormSubmit type="submit" value="Submit" />
      </Form>
    </Wrapper>
  );
}

export default FormCustomer;
