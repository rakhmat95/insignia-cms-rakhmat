import React, { useState, useEffect } from "react";
import DashboardHeader from "../../components/Molecules/DashboardHeader";

import all_orders from "../../constants/orders";
import { calculateRange, sliceData } from "../../utils/table-pagination";

import "../styles.css";
import Table from "react-bootstrap/Table";
import { useDispatch, useSelector } from "react-redux";
import useDebounce from "../../hooks/useDebounce";
import ModalFormOrder from "../../components/Organisms/ModalFormOrder";
import { BASE_URL } from "../../constants";
import styled from "styled-components";
import { BiEdit, BiTrash } from "react-icons/bi";
import Modal from "../../components/Atoms/Modal";

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    width: 300,
    height: 200,
    marginRight: "-50%",
    backgroundColor: "#FFF",
    transform: "translate(-50%, -50%)",
    borderRadius: 10,
  },
};

const Control = styled.td`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

function Orders() {
  const authToken = useSelector((rootState) => rootState.authToken);
  const { order, orders } = useSelector((rootState) => rootState.orders);

  const dispatch = useDispatch();

  const [search, setSearch] = useState("");
  // const [orders, setOrders] = useState(all_orders);
  const [page, setPage] = useState(1);
  const [pagination, setPagination] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const debouncedSearch = useDebounce(search, 500);

  const [openModalAddOrder, setOpenModalAddOrder] = useState(false);
  const [openModalEditOrder, setOpenModalEditOrder] = useState(false);
  const [openModalDeleteOrder, setOpenModalDeleteOrder] = useState(false);

  useEffect(() => {
    setPagination(calculateRange(all_orders, 5));
    // setOrders(sliceData(all_orders, page, 5));
  }, []);

  useEffect(() => {
    dispatch.orders.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
  }, [debouncedSearch]);

  // Search
  const __handleSearch = (event) => {
    setSearch(event.target.value);
    if (event.target.value !== "") {
      let search_results = orders.filter(
        (item) =>
          item.first_name.toLowerCase().includes(search.toLowerCase()) ||
          item.last_name.toLowerCase().includes(search.toLowerCase()) ||
          item.product.toLowerCase().includes(search.toLowerCase())
      );
      // setOrders(search_results);
    } else {
      __handleChangePage(1);
    }
  };

  // Change Page
  const __handleChangePage = (new_page) => {
    setPage(new_page);
    // setOrders(sliceData(all_orders, new_page, 5));
  };

  const onEdit = (data) => {
    setSelectedItem(data);
    setOpenModalEditOrder(true);
  };

  const onDelete = (data) => {
    setSelectedItem(data);
    setOpenModalDeleteOrder(true);
  };

  const onSubmitAdd = async (data) => {
    const response = await dispatch.orders.createOrder({
      token: authToken?.jwt,
      params: { data },
    });
    await dispatch.orders.createOrderDetail({
      token: authToken?.jwt,
      params: {
        data: {
          order_id: response?.id,
          travel_package_id: data?.travel_package_id,
          price: response?.attributes?.total_price,
        },
      },
    });
    dispatch.orders.resetOrdersData();
    await dispatch.orders.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalAddOrder(false);
  };

  const onSubmitEdit = async (data) => {
    const response = await dispatch.orders.updateOrders({
      token: authToken?.jwt,
      params: {
        data: {
          invoice_number: data?.invoice_number,
          customer_id: data?.customer_id,
          total_price: data?.total_price,
        },
      },
      id: selectedItem?.attributes?.order_id?.data?.id,
    });
    await dispatch.orders.updateOrderDetails({
      token: authToken?.jwt,
      params: {
        data: {
          order_id: response?.id,
          travel_package_id: data?.travel_package_id,
          price: response?.attributes?.total_price,
        },
      },
      id: selectedItem?.id,
    });
    dispatch.orders.resetOrdersData();
    await dispatch.orders.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalEditOrder(false);
  };

  const onSubmitDelete = async () => {
    await dispatch.orders.deleteOrder({
      token: authToken?.jwt,
      params: selectedItem?.attributes?.order_id?.data?.id,
    });
    await dispatch.orders.deleteOrderDetail({
      token: authToken?.jwt,
      params: selectedItem?.id,
    });
    dispatch.orders.resetOrdersData();
    await dispatch.orders.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalDeleteOrder(false);
  };

  return (
    <div className="dashboard-content">
      <ModalFormOrder
        title={"Add Order"}
        isOpen={openModalAddOrder}
        closeModal={() => setOpenModalAddOrder(false)}
        onSubmit={onSubmitAdd}
      />
      <ModalFormOrder
        title={"Edit Order"}
        defaultValue={selectedItem?.attributes}
        isOpen={openModalEditOrder}
        closeModal={() => setOpenModalEditOrder(false)}
        onSubmit={onSubmitEdit}
      />
      <Modal
        title={`Are you sure to delete id: ${selectedItem?.id}?`}
        isOpen={openModalDeleteOrder}
        modalStyles={modalStyles}
        closeModal={() => setOpenModalDeleteOrder(false)}
        onConfirm={onSubmitDelete}
      />

      <DashboardHeader
        btnText="Add Order"
        onClick={() => setOpenModalAddOrder(true)}
      />

      <div className="dashboard-content-container">
        <div className="dashboard-content-header">
          <h2>Orders List</h2>
          <div className="dashboard-content-search">
            <input
              type="text"
              value={search}
              placeholder="Search.."
              className="dashboard-content-input"
              onChange={(e) => __handleSearch(e)}
            />
          </div>
        </div>

        <Table responsive>
          <thead>
            <th>ID</th>
            <th>INVOICE NUMBER</th>
            <th>CUSTOMER</th>
            <th>TOTAL PRICE</th>
            <th>TRAVEL PACKAGE</th>
            <th>IMAGE</th>
            <th></th>
          </thead>

          {orders?.length !== 0 ? (
            <tbody>
              {orders?.map((item, index) => (
                <tr key={index}>
                  <td>
                    <span>{item.id}</span>
                  </td>
                  <td>
                    <span>
                      {
                        item.attributes?.order_id?.data?.attributes
                          ?.invoice_number
                      }
                    </span>
                  </td>
                  <td>
                    <span>
                      {
                        item.attributes?.order_id?.data?.attributes?.customer_id
                          ?.data?.attributes?.name
                      }
                    </span>
                  </td>
                  <td>
                    <span>
                      {item.attributes?.order_id?.data?.attributes?.total_price}
                    </span>
                  </td>
                  <td>
                    <span>
                      {
                        item.attributes?.travel_package_id?.data?.attributes
                          ?.name
                      }
                    </span>
                  </td>
                  <td>
                    <img
                      src={
                        BASE_URL +
                        item.attributes.travel_package_id?.data?.attributes
                          ?.image?.data?.attributes?.url
                      }
                      alt="canceled-icon"
                      className="dashboard-content-icon"
                    />
                  </td>
                  <Control>
                    <div onClick={() => onEdit(item)}>
                      <BiEdit />
                    </div>
                    <div onClick={() => onDelete(item)}>
                      <BiTrash />
                    </div>
                  </Control>
                </tr>
              ))}
            </tbody>
          ) : null}
        </Table>

        {orders.length !== 0 ? (
          <div className="dashboard-content-footer">
            {pagination.map((item, index) => (
              <span
                key={index}
                className={item === page ? "active-pagination" : "pagination"}
                onClick={() => __handleChangePage(item)}
              >
                {item}
              </span>
            ))}
          </div>
        ) : (
          <div className="dashboard-content-footer">
            <span className="empty-table">No data</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default Orders;
