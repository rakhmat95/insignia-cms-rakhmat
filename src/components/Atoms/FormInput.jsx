import React from "react";
import styled from "styled-components";

const Input = styled.input`
  display: flex;
  flex: 1;
  padding: 1em;
  margin-bottom: 16px;
  border-radius: 10px;
  width: 100%;
`;

function FormInput(props) {
  const {
    disabled,
    required,
    placeholder,
    type,
    defaultValue,
    value,
    onChange,
    accept,
  } = props;

  return (
    <Input
      {...props}
      disabled={disabled}
      required={required}
      placeholder={placeholder}
      type={type ?? "text"}
      onChange={onChange}
      accept={accept ?? ""}
      value={value}
      defaultValue={defaultValue}
    />
  );
}

export default FormInput;
