import React, { useState, useEffect } from "react";
import DashboardHeader from "../../components/Molecules/DashboardHeader";

import all_orders from "../../constants/orders";
import { calculateRange, sliceData } from "../../utils/table-pagination";

import "../styles.css";
import Table from "react-bootstrap/Table";
import { useDispatch, useSelector } from "react-redux";
import { BiEdit, BiTrash } from "react-icons/bi";
import styled from "styled-components";
import useDebounce from "../../hooks/useDebounce";
import ModalFormCustomer from "../../components/Organisms/ModalFormCustomer";
import Modal from "../../components/Atoms/Modal";

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    width: 300,
    height: 200,
    marginRight: "-50%",
    backgroundColor: "#FFF",
    transform: "translate(-50%, -50%)",
    borderRadius: 10,
  },
};

const Control = styled.td`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

function Customers() {
  const authToken = useSelector((rootState) => rootState.authToken);
  const { customer, customers } = useSelector(
    (rootState) => rootState.customers
  );

  const dispatch = useDispatch();

  const [search, setSearch] = useState("");
  const [orders, setOrders] = useState(all_orders);
  const [page, setPage] = useState(1);
  const [pagination, setPagination] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const debouncedSearch = useDebounce(search, 500);

  const [openModalAddCustomer, setOpenModalAddCustomer] = useState(false);
  const [openModalEditCustomer, setOpenModalEditCustomer] = useState(false);
  const [openModalDeleteCustomer, setOpenModalDeleteCustomer] = useState(false);

  useEffect(() => {
    setPagination(calculateRange(all_orders, 5));
    setOrders(sliceData(all_orders, page, 5));
  }, []);

  useEffect(() => {
    dispatch.customers.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
  }, [debouncedSearch]);

  // Search
  const __handleSearch = (event) => {
    setSearch(event.target.value);
    if (event.target.value !== "") {
      let search_results = orders.filter(
        (item) =>
          item.first_name.toLowerCase().includes(search.toLowerCase()) ||
          item.last_name.toLowerCase().includes(search.toLowerCase()) ||
          item.product.toLowerCase().includes(search.toLowerCase())
      );
      setOrders(search_results);
    } else {
      __handleChangePage(1);
    }
  };

  // Change Page
  const __handleChangePage = (new_page) => {
    setPage(new_page);
    setOrders(sliceData(all_orders, new_page, 5));
  };

  const onEdit = (data) => {
    setSelectedItem(data);
    setOpenModalEditCustomer(true);
  };

  const onDelete = (data) => {
    setSelectedItem(data);
    setOpenModalDeleteCustomer(true);
  };

  const onSubmitAdd = async (data) => {
    await dispatch.customers.create({
      token: authToken?.jwt,
      params: { data },
    });
    dispatch.customers.resetCustomersData();
    await dispatch.customers.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalAddCustomer(false);
  };

  const onSubmitEdit = async (data) => {
    await dispatch.customers.updates({
      token: authToken?.jwt,
      params: { data },
      id: selectedItem?.id,
    });
    dispatch.customers.resetCustomersData();
    await dispatch.customers.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalEditCustomer(false);
  };

  const onSubmitDelete = async () => {
    await dispatch.customers.delete({
      token: authToken?.jwt,
      params: selectedItem?.id,
    });
    dispatch.customers.resetCustomersData();
    await dispatch.customers.find({
      token: authToken?.jwt,
      params: `${debouncedSearch}`,
    });
    setOpenModalDeleteCustomer(false);
  };

  return (
    <div className="dashboard-content">
      <ModalFormCustomer
        title={"Add Customer"}
        isOpen={openModalAddCustomer}
        closeModal={() => setOpenModalAddCustomer(false)}
        onSubmit={onSubmitAdd}
      />
      <ModalFormCustomer
        title={"Edit Customer"}
        defaultValue={selectedItem?.attributes}
        isOpen={openModalEditCustomer}
        closeModal={() => setOpenModalEditCustomer(false)}
        onSubmit={onSubmitEdit}
      />
      <Modal
        title={`Are you sure to delete id: ${selectedItem?.id}?`}
        isOpen={openModalDeleteCustomer}
        modalStyles={modalStyles}
        closeModal={() => setOpenModalDeleteCustomer(false)}
        onConfirm={onSubmitDelete}
      />

      <DashboardHeader
        btnText="Add Customer"
        onClick={() => setOpenModalAddCustomer(true)}
      />

      <div className="dashboard-content-container">
        <div className="dashboard-content-header">
          <h2>Customers List</h2>
          <div className="dashboard-content-search">
            <input
              type="text"
              value={search}
              placeholder="Search.."
              className="dashboard-content-input"
              onChange={(e) => __handleSearch(e)}
            />
          </div>
        </div>

        <Table responsive>
          <thead>
            <th>ID</th>
            <th>NAME</th>
            <th>PHONE</th>
            <th>EMAIL</th>
            <th>ADDRESS</th>
            <th></th>
          </thead>

          {customers?.length !== 0 ? (
            <tbody>
              {customers?.map((item, index) => (
                <tr key={index}>
                  <td>
                    <span>{item.id}</span>
                  </td>
                  <td>
                    <span>{item.attributes.name}</span>
                  </td>
                  <td>
                    <span>{item.attributes.phone}</span>
                  </td>
                  <td>
                    <span>{item.attributes.email}</span>
                  </td>
                  <td>
                    <span>{item.attributes.address}</span>
                  </td>
                  <Control>
                    <div onClick={() => onEdit(item)}>
                      <BiEdit />
                    </div>
                    <div onClick={() => onDelete(item)}>
                      <BiTrash />
                    </div>
                  </Control>
                </tr>
              ))}
            </tbody>
          ) : null}
        </Table>

        {customers.length !== 0 ? (
          <div className="dashboard-content-footer">
            {pagination.map((item, index) => (
              <span
                key={index}
                className={item === page ? "active-pagination" : "pagination"}
                onClick={() => __handleChangePage(item)}
              >
                {item}
              </span>
            ))}
          </div>
        ) : (
          <div className="dashboard-content-footer">
            <span className="empty-table">No data</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default Customers;
