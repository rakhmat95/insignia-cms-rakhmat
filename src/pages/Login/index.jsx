import React from "react";
import FormLogin from "../../components/Molecules/FormLogin";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

function Login(props) {
  const navigate = useNavigate();

  const dispatch = useDispatch();

  const onSubmit = (data) => {
    dispatch.authToken.login(data);
  };

  const onRegister = () => {
    navigate("/register");
  };

  return <FormLogin onSubmit={onSubmit} onRegister={onRegister} />;
}

export default Login;
