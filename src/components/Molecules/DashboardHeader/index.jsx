import React from "react";

import "./styles.css";
import AccountName from "../../Atoms/AccountName";
import { useSelector } from "react-redux";

function DashboardHeader({ btnText, onClick }) {
  const authToken = useSelector((rootState) => rootState.authToken);

  return (
    <div className="dashbord-header-container">
      {btnText && (
        <button className="dashbord-header-btn" onClick={onClick}>
          {btnText}
        </button>
      )}

      <div className="dashbord-header-right">
        <AccountName username={authToken?.user?.username} />
      </div>
    </div>
  );
}

export default DashboardHeader;
