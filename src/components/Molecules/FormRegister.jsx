import React from "react";
import { useForm } from "react-hook-form";
import styled from "styled-components";
import { Colors } from "../../constants";
import FormInput from "../Atoms/FormInput";

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10em;
`;

const Title = styled.h2`
  color: whitesmoke;
  margin-bottom: 16px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2.5em;
  background-color: ${Colors.PRIMARY};
  border-radius: 10px;
  width: 50%;
`;

const FormSubmit = styled.input`
  padding: 10px;
  border-radius: 10px;
  background-color: white;
  font-size: medium;
  font-weight: bold;
`;

const Login = styled.div`
  margin-top: 16px;
  text-decoration: underline;
  color: white;
  font-weight: bold;
`;

function FormRegister({ onSubmit, onLogin }) {
  const { register, handleSubmit, setValue } = useForm();

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Title>Register Akun</Title>
        <FormInput
          required={true}
          {...register("email", { required: true })}
          placeholder="Email"
          type={"email"}
          onChange={(e) => setValue("email", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("username", { required: true })}
          placeholder="Username"
          onChange={(e) => setValue("username", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("password", { required: true })}
          placeholder="Password"
          type={"password"}
          onChange={(e) => setValue("password", e.target.value)}
        />
        <FormSubmit type="submit" value="Register" />
        <Login onClick={onLogin}>Login</Login>
      </Form>
    </Wrapper>
  );
}

export default FormRegister;
