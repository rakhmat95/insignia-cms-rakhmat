import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import SideBarItem from "./sidebar-item";

import "./styles.css";
import logo from "../../../assets/images/white-logo.png";
import LogoutIcon from "../../../assets/icons/logout.svg";
import Logo from "../../Atoms/Logo";
import Modal from "../../Atoms/Modal";
import { useDispatch } from "react-redux";

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    width: 300,
    height: 200,
    marginRight: "-50%",
    backgroundColor: "#FFF",
    transform: "translate(-50%, -50%)",
    borderRadius: 10,
  },
};

function SideBar({ menu }) {
  const location = useLocation();
  const navigate = useNavigate();

  const dispatch = useDispatch();

  const [active, setActive] = useState(1);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    menu.forEach((element) => {
      if (location.pathname === element.path) {
        setActive(element.id);
      }
    });
  }, [location.pathname]);

  const __navigate = (id) => {
    setActive(id);
  };

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  function logout() {
    closeModal();
    dispatch.authToken.logout();
    navigate("/");
  }

  return (
    <nav className="sidebar">
      <Modal
        title={"Are you sure?"}
        isOpen={isOpen}
        modalStyles={modalStyles}
        closeModal={closeModal}
        onConfirm={logout}
      />

      <div className="sidebar-container">
        <div className="sidebar-logo-container">
          <Logo />
        </div>

        <div className="sidebar-container">
          <div className="sidebar-items">
            {menu.map((item, index) => (
              <div key={index} onClick={() => __navigate(item.id)}>
                <SideBarItem active={item.id === active} item={item} />
              </div>
            ))}
          </div>

          <div className="sidebar-footer" onClick={openModal}>
            <span className="sidebar-item-label">Logout</span>
            <img
              src={LogoutIcon}
              alt="icon-logout"
              className="sidebar-item-icon"
            />
          </div>
        </div>
      </div>
    </nav>
  );
}

export default SideBar;
