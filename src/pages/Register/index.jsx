import React from "react";
import FormRegister from "../../components/Molecules/FormRegister";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

function Register(props) {
  const navigate = useNavigate();

  const dispatch = useDispatch();

  const onSubmit = (data) => {
    dispatch.authToken.register(data);
  };

  const onLogin = () => {
    navigate("/");
  };

  return <FormRegister onSubmit={onSubmit} onLogin={onLogin} />;
}

export default Register;
