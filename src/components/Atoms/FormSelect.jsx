import React from "react";
import styled from "styled-components";

const Select = styled.select`
  display: flex;
  flex: 1;
  padding: 1em;
  margin-bottom: 16px;
  border-radius: 10px;
`;

const Option = styled.option`
  display: flex;
  flex: 1;
  padding: 10px;
  margin-bottom: 16px;
  border-radius: 10px;
`;

function FormSelect(props) {
  const { required, placeholder, options, defaultValue, onChange, value } =
    props;

  return (
    <Select
      {...props}
      required={required}
      placeholder={placeholder}
      onChange={onChange}
      defaultChecked
      defaultValue={defaultValue}
    >
      <Option value={""}>{placeholder}</Option>
      {options?.map((item, index) => {
        return (
          <Option key={index} value={item?.value}>
            {item?.label}
          </Option>
        );
      })}
    </Select>
  );
}

export default FormSelect;
