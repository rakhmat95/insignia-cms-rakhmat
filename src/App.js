import React, { useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import SideBar from "./components/Molecules/Sidebar";
import sidebar_menu from "./constants/sidebar-menu";

import "./App.css";
import Orders from "./pages/Orders";
import TravelPackages from "./pages/TravelPackages";
import Customers from "./pages/Customers";
import { useSelector } from "react-redux";
import Register from "./pages/Register";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";

function App() {
  const authToken = useSelector((rootState) => rootState.authToken);

  return (
    <Router>
      <div className="dashboard-container">
        {authToken && <SideBar menu={sidebar_menu} />}

        <div className="dashboard-body">
          <Routes>
            {authToken ? (
              <>
                <Route path="*" element={<Orders />} />
                <Route exact path="/orders" element={<Orders />} />
                <Route
                  exact
                  path="/travel-packages"
                  element={
                    <div>
                      <TravelPackages />
                    </div>
                  }
                />
                <Route
                  exact
                  path="/customers"
                  element={
                    <div>
                      <Customers />
                    </div>
                  }
                />
              </>
            ) : (
              <>
                <Route path="*" element={<NotFound />} />
                <Route exact path="/register" element={<Register />} />
                <Route exact path="/" element={<Login />} />
              </>
            )}
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
