import DashboardIcon from "../assets/icons/dashboard.svg";
import ShippingIcon from "../assets/icons/shipping.svg";
import ProductIcon from "../assets/icons/product.svg";
import UserIcon from "../assets/icons/user.svg";

const sidebar_menu = [
  {
    id: 1,
    icon: ProductIcon,
    path: "/orders",
    title: "Orders",
  },
  {
    id: 2,
    icon: ShippingIcon,
    path: "/travel-packages",
    title: "Travel Packages",
  },
  {
    id: 3,
    icon: UserIcon,
    path: "/customers",
    title: "Customers",
  },
];

export default sidebar_menu;
