import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import FormInput from "../Atoms/FormInput";
import { BASE_URL, Colors } from "../../constants";
import { useSelector } from "react-redux";
import { upload } from "../../api/utils";

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

const Title = styled.h2`
  color: whitesmoke;
  margin-bottom: 16px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${Colors.PRIMARY};
  border-radius: 10px;
  width: 50%;
`;

const FormSubmit = styled.input`
  padding: 10px;
  border-radius: 10px;
  background-color: white;
  font-size: medium;
  font-weight: bold;
`;

const WrapperImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-bottom: 16px;
`;

const ImagePreview = styled.img`
  width: 100px;
  height: 100px;
`;

function FormTravel({ title, defaultValue, onSubmit }) {
  const authToken = useSelector((rootState) => rootState.authToken);

  const { setValue, register, handleSubmit } = useForm();
  const [image, setImage] = useState(null);

  useEffect(() => {
    if (defaultValue) {
      setValue("name", defaultValue?.name);
      setValue("description", defaultValue?.description);
      setValue("price", defaultValue?.price);

      if (defaultValue?.image?.data?.attributes) {
        let defaultImage = { ...defaultValue?.image?.data?.attributes };
        defaultImage.id = defaultValue?.image?.data?.id;
        setValue("image", defaultImage);
        setImage([defaultImage]);
      }
    }
  }, [defaultValue]);

  useEffect(() => {
    if (image?.length > 0) {
      setValue("image", image[0]);
    }
  }, [image]);

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Title>{title}</Title>
        <FormInput
          required={true}
          {...register("name", { required: true })}
          placeholder="Name"
          defaultValue={defaultValue?.name}
          onChange={(e) => setValue("name", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("description", { required: true })}
          placeholder="Description"
          defaultValue={defaultValue?.description}
          onChange={(e) => setValue("description", e.target.value)}
        />
        <FormInput
          required={true}
          {...register("price", { required: true })}
          placeholder="Price"
          defaultValue={defaultValue?.price}
          onChange={(e) => setValue("price", e.target.value)}
        />
        <WrapperImage>
          <FormInput
            required={defaultValue?.image?.data?.attributes ? false : true}
            {...register("image", { required: true })}
            placeholder="Image"
            type={"file"}
            accept={"image/*"}
            onChange={async (e) => {
              const response = await upload({
                token: authToken?.jwt,
                params: e.target.files[0],
              });
              setImage(response);
            }}
          />
          {image?.length > 0 && <ImagePreview src={BASE_URL + image[0]?.url} />}
        </WrapperImage>
        <FormSubmit type="submit" value="Submit" />
      </Form>
    </Wrapper>
  );
}

export default FormTravel;
