import React from "react";
import styled from "styled-components";

const Wrapper = styled.section`
  padding: 1em;
`;

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: #ffffff;
`;

function Logo(props) {
  return (
    <Wrapper>
      <Title>Insignia CMS</Title>
    </Wrapper>
  );
}

export default Logo;
