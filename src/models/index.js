import { authToken } from "./Persisted/authToken";
import { customers } from "./customers";
import { orders } from "./orders";
import { travels } from "./travels";

export const models = {
  authToken,
  customers,
  orders,
  travels,
};
