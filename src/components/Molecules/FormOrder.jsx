import React, { useEffect, useLayoutEffect, useState } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import FormInput from "../Atoms/FormInput";
import FormSelect from "../Atoms/FormSelect";
import { Colors } from "../../constants";
import { useDispatch, useSelector } from "react-redux";

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

const Title = styled.h2`
  color: whitesmoke;
  margin-bottom: 16px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${Colors.PRIMARY};
  border-radius: 10px;
  width: 50%;
`;

const FormSubmit = styled.input`
  padding: 10px;
  border-radius: 10px;
  background-color: white;
  font-size: medium;
  font-weight: bold;
`;

function FormOrder({ title, defaultValue, onSubmit }) {
  const authToken = useSelector((rootState) => rootState.authToken);
  const { customers } = useSelector((rootState) => rootState.customers);
  const { travels } = useSelector((rootState) => rootState.travels);

  const dispatch = useDispatch();

  const { setValue, register, handleSubmit } = useForm();
  const [optionsCustomer, setOptionsCustomer] = useState([]);
  const [optionsTravels, setOptionsTravels] = useState([]);

  const [tempValue, setTempValue] = useState({
    customer_id:
      defaultValue?.order_id?.data?.attributes?.customer_id?.data?.id ?? "",
    travel_package_id: defaultValue?.travel_package_id?.data?.id ?? "",
    total_price: defaultValue?.order_id?.data?.attributes?.total_price ?? "",
    invoice_number:
      defaultValue?.order_id?.data?.attributes?.invoice_number ?? "",
  });

  useLayoutEffect(() => {
    if (customers?.length === 0) {
      dispatch.customers.find({
        token: authToken?.jwt,
        params: "",
      });
    }
  }, [customers]);

  useLayoutEffect(() => {
    if (travels?.length === 0) {
      dispatch.travels.find({
        token: authToken?.jwt,
        params: "",
      });
    }
  }, [travels]);

  useEffect(() => {
    if (defaultValue) {
      setTempValue(tempValue);
    }
  }, [defaultValue]);

  useEffect(() => {
    if (tempValue) {
      setValue("customer_id", tempValue.customer_id);
      setValue("travel_package_id", tempValue.travel_package_id);
      setValue("total_price", tempValue.total_price);
      setValue("invoice_number", tempValue.invoice_number);
    }
  }, [tempValue]);

  useEffect(() => {
    let newOptions = [];
    customers?.map((item, index) => {
      newOptions.push({
        label: `${item?.attributes?.name} - ${item?.attributes?.email}`,
        value: item?.id,
      });
    });
    setOptionsCustomer(newOptions);
  }, [customers]);

  useEffect(() => {
    let newOptions = [];
    travels?.map((item, index) => {
      newOptions.push({
        label: `${item?.attributes?.name} - ${item?.attributes?.price}`,
        value: JSON.stringify({ id: item?.id, price: item?.attributes?.price }),
      });
    });
    setOptionsTravels(newOptions);
  }, [travels]);

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Title>{title}</Title>
        <FormSelect
          required={true}
          {...register("customer_id", { required: true })}
          placeholder="Choose Customer..."
          value={tempValue.customer_id}
          onChange={(e) => {
            setValue("customer_id", e.target.value);
            setTempValue((prev) => ({ ...prev, customer_id: e.target.value }));
          }}
          options={optionsCustomer}
        />
        <FormSelect
          required={true}
          {...register("travel_package_id", { required: true })}
          placeholder="Choose Travel..."
          value={JSON.stringify({
            id: tempValue.travel_package_id,
            price: tempValue.total_price,
          })}
          onChange={(e) => {
            const data = JSON.parse(e.target.value);
            setValue("travel_package_id", data?.id);
            setValue("total_price", data?.price);
            setTempValue((prev) => ({
              ...prev,
              travel_package_id: data?.id,
              total_price: data?.price,
            }));
          }}
          options={optionsTravels}
        />
        <FormInput
          disabled
          required={true}
          {...register("total_price", { required: true })}
          placeholder="Total Price"
          value={tempValue.total_price}
        />
        <FormInput
          required={true}
          {...register("invoice_number", { required: true })}
          placeholder="Invoice Number"
          value={tempValue.invoice_number}
          onChange={(e) => {
            setValue("invoice_number", e.target.value);
            setTempValue((prev) => ({
              ...prev,
              invoice_number: e.target.value,
            }));
          }}
        />
        <FormSubmit type="submit" value="Submit" />
      </Form>
    </Wrapper>
  );
}

export default FormOrder;
