import { init } from "@rematch/core";
import immerPlugin from "@rematch/immer";
import persistPlugin from "@rematch/persist";
import loadingPlugin from "@rematch/loading";
import storage from "redux-persist/lib/storage";
import { models } from "./models";

const persistConfig = {
  key: "root",
  whitelist: ["authToken"],
  storage,
};

const store = init({
  models,
  redux: {
    rootReducers: { LOGOUT: () => undefined },
  },
  plugins: [persistPlugin(persistConfig), loadingPlugin(), immerPlugin()],
});

export default store;
