import axios from "axios";
import { API_URL } from "../constants";

import { configAuth, handler } from "./utils";

export async function create(data) {
  try {
    const res = await axios.post(
      `${API_URL}/travel-packages`,
      data?.params,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function find(data) {
  try {
    let params = "";
    if (data?.params !== "") {
      params = `filters[$or][0][name][$containsi]=${
        data?.params
      }&filters[$or][1][price][$containsi]=${
        isNaN(data?.params) ? "O" : data?.params
      }&filters[$or][2][description][$containsi]=${data?.params}`;
    }

    const res = await axios.get(
      `${API_URL}/travel-packages?populate=image&${params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function findOne(data) {
  try {
    const res = await axios.get(
      `${API_URL}/travel-packages?populate=image&${data?.params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function updates(data) {
  try {
    const res = await axios.put(
      `${API_URL}/travel-packages/${data?.id}`,
      data?.params,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function remove(data) {
  try {
    const res = await axios.delete(
      `${API_URL}/travel-packages/${data?.params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}
