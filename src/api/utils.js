import axios from "axios";
import { API_URL } from "../constants";

export const config = (contentType) => ({
  headers: {
    "Content-Type": contentType ?? "application/json",
  },
});

export const configAuth = (token, contentType) => ({
  headers: {
    Authorization: "Bearer " + token,
    "Content-Type": contentType ?? "application/json",
  },
});

export function handler(err) {
  let error = err;

  if (err.response && err.response.data.hasOwnProperty("message"))
    error = err.response.data;
  else if (!err.hasOwnProperty("message")) error = err.toJSON();

  alert(error.message);

  return new Error(error.message);
}

export async function getImage(data) {
  try {
    const res = await axios.get(
      `${API_URL}/upload/files/${data?.params}`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function getImages(data) {
  try {
    const res = await axios.get(
      `${API_URL}/upload/files`,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function upload(data) {
  try {
    const formData = new FormData();

    formData.append("files", data?.params);

    const res = await axios.post(
      `${API_URL}/upload`,
      formData,
      configAuth(data?.token)
    );

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}
