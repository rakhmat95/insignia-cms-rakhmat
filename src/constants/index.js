const Colors = {
  PRIMARY: "#719e32",
};

const API_URL = "http://localhost:1337/api";
const BASE_URL = "http://localhost:1337";

export { Colors, API_URL, BASE_URL };
