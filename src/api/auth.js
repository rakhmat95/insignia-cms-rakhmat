import axios from "axios";
import { API_URL } from "../constants";

import { handler } from "./utils";

export async function login(data) {
  try {
    const res = await axios.post(`${API_URL}/auth/local`, data);

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}

export async function register(data) {
  try {
    const res = await axios.post(`${API_URL}/auth/local/register`, data);

    return res.data;
  } catch (e) {
    throw handler(e);
  }
}
