import React from "react";
import styled from "styled-components";

const Wrapper = styled.section`
  cursor: pointer;
  border-radius: 20px;
  justify-content: center;
`;

const Title = styled.h1`
  font-size: 18px;
  text-align: center;
  color: #7b809a;
`;

function AccountName({ username }) {
  return (
    <Wrapper>
      <Title>Welcome, {username}</Title>
    </Wrapper>
  );
}

export default AccountName;
