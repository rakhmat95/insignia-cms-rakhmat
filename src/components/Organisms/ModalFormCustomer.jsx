import React, { useState } from "react";
import ReactModal from "react-modal";
import { AiFillCloseCircle } from "react-icons/ai";
import FormCustomer from "../Molecules/FormCustomer";
import { Colors } from "../../constants";

function ModalFormCustomer({
  title,
  defaultValue,
  isOpen,
  modalStyles,
  closeModal,
  onSubmit,
}) {
  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={closeModal}
      style={
        modalStyles ?? {
          content: {
            display: "flex",
            flex: 1,
            flexDirection: "row",
            backgroundColor: Colors.PRIMARY,
            borderRadius: 16,
            justifyContent: "center",
            marginRight: 100,
            marginLeft: 100,
          },
        }
      }
    >
      <div onClick={closeModal}>
        <AiFillCloseCircle size={36} color={"white"} />
      </div>
      <FormCustomer
        defaultValue={defaultValue}
        title={title}
        onSubmit={onSubmit}
      />
    </ReactModal>
  );
}

export default ModalFormCustomer;
