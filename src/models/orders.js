import { createModel } from "@rematch/core";
import * as api from "../api/orders";

export const orders = createModel()({
  state: {
    order: null,
    orders: [],
  },
  reducers: {
    updateOrderData(state, payload) {
      state.order = payload;
      return state;
    },
    updateOrdersData(state, payload) {
      state.orders = payload;
      return state;
    },
    resetOrderData(state) {
      state.order = null;
      return state;
    },
    resetOrdersData(state) {
      state.orders = [];
      return state;
    },
  },
  effects: (dispatch) => ({
    async createOrder({ token, params }) {
      const response = await api.createOrder({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
    async createOrderDetail({ token, params }) {
      const response = await api.createOrderDetail({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
    async find({ token, params }) {
      const response = await api.find({
        token: token,
        params: params,
      });
      const data = response?.data;
      dispatch.orders.updateOrdersData(data);
      return data;
    },
    async findOne({ token, params }) {
      const response = await api.findOne({
        token: token,
        params: params,
      });
      const data = response?.data;
      dispatch.orders.updateOrderData(data);
      return data;
    },
    async updateOrders({ token, params, id }) {
      const response = await api.updateOrders({
        token: token,
        params: params,
        id: id,
      });
      const data = response?.data;
      return data;
    },
    async updateOrderDetails({ token, params, id }) {
      const response = await api.updateOrderDetails({
        token: token,
        params: params,
        id: id,
      });
      const data = response?.data;
      return data;
    },
    async deleteOrder({ token, params }) {
      const response = await api.removeOrders({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
    async deleteOrderDetail({ token, params }) {
      const response = await api.removeOrderDetails({
        token: token,
        params: params,
      });
      const data = response?.data;
      return data;
    },
  }),
});
